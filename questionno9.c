#include <stdio.h> // write a program to convert fahrenheit to celsius
int main()
{
    float Fahrenheit, Celsius;

    printf("Input Fahrenheit = ");
    scanf("%f", &Fahrenheit);
    Celsius = ((Fahrenheit - 32) * 5) / 9;
    printf("\n Temperature in Celsius is : %.3f", Celsius);
    return (0);
}