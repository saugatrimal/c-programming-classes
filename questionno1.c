#include <stdio.h> //Write a program to find the area and perimeter of the circle

int main()
{
    int radius;
    float pi = 3.14;

    printf(" Enter the radius of circle = "); //example = 3
    scanf("%d", &radius);

    printf(" The area of circle is %.3f\n", pi * radius * radius); // ans = 28.260

    printf(" The perimeter of circle is %.3f\n", 2 * pi * radius); // ans = 18.840

    return 0;
}