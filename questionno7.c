#include <stdio.h>
#include <math.h>

int main()
{
    int a, b, sum, sub, mul, div;
    printf("Write two numbers = ");
    scanf("%d %d", &a, &b);
    sum = a + b;
    sub = a - b;
    mul = a * b;
    div = a / b;

    printf("\nThe sum of a and b are = %d", sum);
    printf("\nThe substraction of a and b are = %d", sub);
    printf("\nThe multiplication of a and b are = %d", mul);
    printf("\nThe division of a and b are = %d", div);

    return 0;
}