
#include <stdio.h> //WAP TO FIND THE LAST DIGIT OF INTEGER.

int main()

{

    int num, digit;
    printf("Enter a number:");
    scanf("%d", &num);

    digit = num % 10000; // Find last digit of a number
    printf("Last digit: %d", digit);

    return 0;
}
