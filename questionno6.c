#include <stdio.h> // Write a program to find the area of triangle when its three sides are given
#include <math.h>

int main()
{
    double a, b, c, s, area;

    printf("Enter sides of a triangle = ");
    scanf("%lf %lf %lf", &a, &b, &c);

    s = (a + b + c) / 2;

    area = sqrt(s * (s - a) * (s - b) * (s - c));

    printf("Area of the triangle = %.4lf\n", area);

    return 0;
}
