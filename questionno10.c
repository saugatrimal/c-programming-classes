#include <stdio.h> //write a program to find the area of sphere
int main()
{
    int radius;
    printf("Enter the radius of sphere = ");
    scanf("%d", &radius);
    double pie = 3.14285714286;
    double area_sphere = 4 * pie * (radius * radius);
    printf("Surface area of the sphere= %lf", area_sphere);
}