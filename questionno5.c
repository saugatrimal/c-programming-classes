#include <stdio.h> //WAP TO FIND  THE ARE OF RIGHT ANGLE TRIANGLE.

int main()
{
    float b, h;
    printf("Write the length of base = ");
    scanf("%f", &b);
    printf("Write the length of height = ");
    scanf("%f", &h);

    printf("The area of right angle triangle is = %f ", (b * h) / 2);

    return 0;
}